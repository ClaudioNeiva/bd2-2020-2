create table funcionario (
	matricula 	serial			not null,
	nome		varchar(100)	not null,
	email		varchar(255)	not null,
	salario		numeric(10,2)	not null,
	constraint pk_funcionario
		primary key (matricula));

insert into funcionario (nome, email, salario) values
	('claudio neiva','a@ucsal.br',30000),
	('maria da silva','b@ucsal.br',25000),
	('joaquim josé','c@ucsal.br',2000),
	('clara santana','d@ucsal.br',3000),
	('joao silva','e@ucsal.br',5000);

revoke all on all tables in schema public from gerente_rh cascade; 
revoke all on all sequences in schema public from gerente_rh cascade; 
revoke all on all functions in schema public from gerente_rh cascade; 
revoke all on schema public from gerente_rh cascade; 
alter default privileges in schema public revoke all on tables from gerente_rh;
drop role if exists gerente_rh;

revoke all on funcionario,funcionario_matricula_seq from gerente_rh;
drop role if exists gerente_rh; 

create role gerente_rh
	noinherit;
grant select, update, insert on funcionario to gerente_rh;
grant usage on funcionario_matricula_seq to gerente_rh;
grant gerente_rh to claudio;
revoke gerente_rh from claudio;

grant update(situacao), insert, select on pedido to web_venda;

drop role claudio;
create role claudio 
	password 'caju123'
	login
	noinherit;	

drop role assistente_rh;
create role assistente_rh
	NOINHERIT;
-- selecionar matricula, nome e salario, mas apenas para os funcionários com salário inferior à 10000!
create view v_funcionario 
as
	select	matricula,
			nome, 
			salario
	from	funcionario
	where 	salario < 10000
;
grant select on v_funcionario to assistente_rh;
--revoke select on v_funcionario from assistente_rh;
grant assistente_rh to claudio;
--revoke assistente_rh from claudio;
grant gerente_rh to claudio;

select	*
from	pg_roles
where	rolcanlogin = false;

select *
from 	pg_user;

alter table funcionario
	add column login varchar(40) null;
select *
from funcionario;
update funcionario
set	login = 'claudio'
where nome = 'claudio neiva';
drop view if exists v_funcionario_identificado ;
create view v_funcionario_identificado 
as
	select	*
	from	funcionario
	where 	login = current_user
;
grant select on v_funcionario_identificado to public;



------ como usuário claudio

select *
from v_funcionario_identificado;

set role assistente_rh;
select *
from v_funcionario;

set role gerente_rh;
select *
from funcionario;

update funcionario
set	salario = 31000
where matricula = 1;

insert into funcionario (matricula, nome, email, salario) values
	(100, 'leila','f@ucsal.br',40000);
	
insert into funcionario (nome, email, salario) values
	('manuela','g@ucsal.br',38000);