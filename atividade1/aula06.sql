create table veiculo (
	placa			char(7)			not null,
	ano_fabricacao	integer			not null,
	data_aquisicao	date			not null,
	data_venda		date			 	null,
	km_atual		integer			not null,
	chassi			char(17)		not null,
	codigo_modelo	integer			not null,
	codigo_grupo	integer			not null,
	constraint pk_veiculo
		primary key (placa),
	constraint un_veiculo_chassi
		unique (chassi));

alter table veiculo
	add constraint ch_veiculo_data_venda
		check (data_venda >= data_aquisicao);

select *
from veiculo
where placa = 'ABC4567';

update veiculo
set data_venda = '2019-05-15'
where placa = 'ABC4567';


-- Views

-- View fabricante e modelo

drop view if exists v_fabricante_modelo;

create or replace view v_fabricante_modelo
as
select 	f.cnpj,
		f.nome as nome_fabricante,
		f.telefone,
		coalesce(m.nome,'(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante);

select nome_fabricante
from v_fabricante_modelo
where nome_modelo = 'Cruze';


-- View modelo (para atualização)

select * 
from modelo;

create or replace view v_modelo
as
select	nome,
		cnpj_fabricante
from	modelo
where	cnpj_fabricante in ('59275792000150','74779200015077')
WITH LOCAL CHECK OPTION;		

select *
from v_modelo;

insert into v_modelo (nome, cnpj_fabricante) values 
 ('Gurgel','5BCE57920001A0');
 
select *
from modelo;

-- View materialized

create materialized view vm_fabricante_modelo
as
select 	f.cnpj,
		f.nome as nome_fabricante,
		f.telefone,
		coalesce(m.nome,'(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante);

select *
from vm_fabricante_modelo;

update fabricante
set nome = 'Volkswagem do Brasil SA'
where nome = 'Volkswagem';

select *
from fabricante;

select *
from vm_fabricante_modelo;

REFRESH MATERIALIZED VIEW vm_fabricante_modelo;

select *
from vm_fabricante_modelo;



-- Não faça assim, pois view materializada é melhor:

CREATE TABLE tab_modelo_fabricante AS 
select 	f.cnpj,
		f.nome as nome_fabricante,
		f.telefone,
		coalesce(m.nome,'(sem modelo cadastrado)') as nome_modelo
from fabricante f
left outer join modelo m on (f.cnpj = m.cnpj_fabricante);

select *
from tab_modelo_fabricante;



-- Custo da operação

CREATE EXTENSION file_fdw;

CREATE SERVER local_file FOREIGN DATA WRAPPER file_fdw;

CREATE FOREIGN TABLE words (word text NOT NULL)
SERVER local_file
OPTIONS (filename '/usr/share/dict/words');

CREATE MATERIALIZED VIEW wrd AS SELECT * FROM words;

CREATE UNIQUE INDEX wrd_word ON wrd (word);

select *
from words
where word = 'caju';
-- cost=1898.81

select *
from wrd
where word = 'caju';
-- cost=1797.01
-- cost=8.44 


-- Trigger

CREATE OR REPLACE FUNCTION f_trg_upd_fabricante()
RETURNS trigger 
AS $$
BEGIN
    if length(new.telefone) < 8 then
		raise exception SQLSTATE '22000' USING HINT = 'Telefone inválido!';
	end if;
	raise notice 'cnpj antes % ; cnpj depois %', OLD.cnpj, NEW.cnpj;
	raise notice 'telefone antes % ; telefone depois %', OLD.telefone, NEW.telefone;
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- O controle de tamanho mínimo para o campo telefone é melhor implementado utilizando a constraint de check
-- alter table fabricante
-- 	add constraint ch_fabricante_telefone
-- 		check (length(telefone)>=8);

CREATE TRIGGER trg_upd_fabricante
BEFORE UPDATE or INSERT or DELETE
ON fabricante
FOR EACH ROW
EXECUTE PROCEDURE f_trg_upd_fabricante();

select *
from fabricante;

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"43563465     "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"946756756    "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante
set telefone = '71' || telefone
where cnpj in ('74779200015077','5BCE57920001A0');
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

-- OLD
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"7143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"71946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"
update fabricante
set telefone = '55' || telefone
where cnpj in ('74779200015077','5BCE57920001A0');
-- NEW
-- "5BCE57920001A0"	"Veículos do Brasil SA"	"557143563465   "	"Av. Feira"	"2769"	"Edf. Qualque Coisa"	"Liberdade"	"FDS"
-- "74779200015077"	"Tesla do Brasil SA"	"5571946756756  "	"Av. Bahia"	"5656"	"Edf. Uma Coisa"	"Moca"	"FDS"

select CURRENT_USER, CURRENT_TIMESTAMP, CURRENT_TIME, CURRENT_DATE;

update fabricante
set telefone = '55123'
where cnpj = '74779200015077';

select *
from fabricante;

-- cod_item, qtd, valor_untario, valor_total
-- movimento (data, operacao[D|C], valor)
-- saldo atual = saldo inicial + acumulado de todas as operações (movimentos)
