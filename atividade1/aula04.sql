-- Listar a quantidade de veículos por ano de fabricação, mas apenas para os anos de fabricação com 3 ou mais veículos
-- e para veículos com menos de 90000 km rodados;

select	ano_fabricacao,
		count(*)
from	veiculo 
where	km_atual < 90000
group by ano_fabricacao
having count(*) >= 3;


-- Listar os veículos;
-- Saída: placa, ano_fabricacao e o nome do modelo;
-- Filtro: veículos com menos de 90000 km rodados.

select	v.placa,
		v.ano_fabricacao,
		m.nome
from	veiculo v
		inner join modelo m on (m.codigo = v.codigo_modelo)
where	v.km_atual < 90000;


-- Listar veículos e acessórios;
-- Saída: placa, ano de fabricação, nome do modelo e o nome do acessório ('(sem acessório)' para veículos que não possuem nenhum acessório);
-- Filtro: ano de fabricação maior ou igual à 2018.
-- Obs (desnecessária!!!): Os veículos sem acessório devem ser retornados!

select v.placa,
	   v.ano_fabricacao,
	   m.nome as nome_modelo,
	   coalesce(a.nome,'(sem acessório)') as nome_acessorio
from veiculo as v
	inner join modelo as m on (m.codigo = v.codigo_modelo)	
	left outer join veiculo_acessorio as va on (va.placa_veiculo = v.placa)
	left outer join acessorio as a on (a.sigla = va.sigla_acessorio)
where v.ano_fabricacao >= 2018;

select v.placa,
	   v.ano_fabricacao,
	   m.nome as nome_modelo,
	   coalesce(a.nome,'(sem acessório)') as nome_acessorio
from veiculo_acessorio as va 
	inner join acessorio as a on (a.sigla = va.sigla_acessorio)
	right outer join veiculo as v on (va.placa_veiculo = v.placa)
	right outer join modelo as m on (m.codigo = v.codigo_modelo)	
where v.ano_fabricacao >= 2018;


-- Listar veículos e acessórios;
-- Saída: placa ('(não instalado em nenhum veículo)' para acessórios que não estão instalados em nenhum veículo e
-- a sigla do acessório ('(sem acessório)' para veículos que não possuem nenhum acessório);
-- Exemplo de saída:
-- PLACA								NOME_ACESSORIO
-- ABC-1234								Ar condicionado
-- ABC-1234								Direção hidráulica
-- CDE-2342								(sem acessório)
-- (não instalado em nenhum veículo)	Computador de bordo

select 	coalesce(v.placa, '(Não instalado em nenhum veículo)') as placa,
	   	coalesce(a.nome, '(Sem acessório)') as nome_acessorio
from	veiculo as v
	full join veiculo_acessorio as va on (va.placa_veiculo = v.placa)
	full join acessorio as a on (a.sigla = va.sigla_acessorio)


-- Listar dos os modelos e todos os acessórios, combinando todas as possibilidades;
-- Saída: nome do modelo e sigla_acessório.

select	m.nome,
		a.sigla
from	modelo m,
		acessorio a;

select	m.nome,
		a.sigla
from	modelo m
		cross join acessorio a;


-- USAR O NATURAL JOIN
-- Listar os veículos;
-- Saída: placa, ano_fabricacao e o nome do modelo;
-- Filtro: veículos com menos de 90000 km rodados.

select	v.placa,
		v.ano_fabricacao,
		m.nome,
		m.codigo_modelo,
		v.codigo_modelo
from	veiculo v
		inner join modelo m on (v.codigo_modelo = m.codigo_modelo)
where	v.km_atual < 90000;

select	v.placa,
		v.ano_fabricacao,
		m.nome
from	veiculo v
		natural inner join modelo m
where	v.km_atual < 90000;

alter table modelo
	rename column codigo to codigo_modelo;
	
select 	*
from 	modelo;

select	*
from	veiculo;

-- Dados adicionais para a próxima consulta:

drop table if exists funcionario cascade;
drop table if exists dependente cascade;

create table funcionario (
	cpf				char(11)		not null,
	nome			varchar(40)		not null,
	telefone		char(13)		not null,
	constraint pk_funcionario
		primary key (cpf));

create table dependente (
	cpf_funcionario	char(11)		not null,
	cpf				char(11)		not null,
	nome			varchar(40)		not null,
	telefone		char(13)		not null,
	constraint pk_dependente
		primary key (cpf),
	constraint fk_dependente_funcionario
		foreign key (cpf_funcionario)
		references funcionario);

insert into funcionario (cpf, nome, telefone)
values
	('12345678912','Antonio','71828282822'),
	('23434545678','Claudio','71525252525'),
	('45454544433','Neiva','716262626262');

insert into dependente (cpf_funcionario, cpf, nome, telefone)
values
	('12345678912','23434543456','Maria'  ,'75838478574'),
	('12345678912','87547575675','Pedro'  ,'71664477338'),
	('23434545678','98398384844','Joaquim','716476467444'),
	('23434545678','23423423444','Carla'  ,'737847474744'),
	('45454544433','11111111111','Claudio','717756533221');

select	*
from	funcionario;

select	*
from	dependente;

-- Listar os nomes e telefones, de funcionários e dependentes, construindo uma lista única, por ordem crescente de nome.
-- Exemplo da saída:
-- NOME				TELEFONE	
-- Antonio			71828282822
-- Carla			737847474744
-- Claudio			71525252525
-- Claudio			717756533221
-- Joaquim			716476467444
-- Maria			75838478574
-- Neiva			716262626262
-- Pedro			71664477338

select nome, 
	   telefone 
from funcionario 
union 
select nome,
	   telefone 
from dependente
order by 1 asc;


-- Listar os nomes, de funcionários e dependentes, por ordem crescente de nome, para funcionar como uma lista de presença para assinatura.
-- Exemplo da saída:
-- NOME				
-- Antonio			
-- Carla			
-- Claudio			
-- Claudio			
-- Joaquim			
-- Maria			
-- Neiva			
-- Pedro			

select nome
from funcionario 
union 
select nome
from dependente
order by 1 asc;

select nome
from funcionario 
union all
select nome
from dependente
order by 1 asc;


-- Todos os acessórios que não estão instalados em nenhum veículo.
-- Saída: sigla e o nome do acessório.

select 	a.sigla,
		a.nome
from	acessorio a
where	a.sigla  not in (select	va.sigla_acessorio
					   	from 	veiculo_acessorio va);

-- Todos os acessórios que estão instalados em nenhum veículo.
-- Saída: sigla e o nome do acessório.

select 	a.sigla,
		a.nome
from	acessorio a
where	a.sigla  in (select	va.sigla_acessorio
				   	from 	veiculo_acessorio va);
					
-- Solução que não comunica bem o objetivo da query:
select 	distinct a.sigla,
		a.nome
from	acessorio a
		inner join veiculo_acessorio va on (va.sigla_acessorio = a.sigla);

-- Solução que provoca um queda de performance (você NÃO deve fazer assim):
select 	a.sigla,
		a.nome
from	acessorio a						
where	exists (select	*
				from 	veiculo_acessorio va
			   where	va.sigla_acessorio = a.sigla);







select	placa, 
		ano_fabricacao, 
		m.nome 
from	veiculo 
	left join modelo m 
	on veiculo.codigo_modelo = m.codigo
where	km_atual < 90000
;







	('ABC1234',2017,'2018-10-05',null,			80015,'23445643FGX23345',(select codigo from modelo where nome = 'Ka'), (select codigo from grupo where nome = 'Básico')),
	('ABC4567',2018,'2019-06-08',null,			53025,'3459847325ASD328',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Básico')),
	('CDE7654',2018,'2018-11-02','2020-01-07',	45800,'2344JH122H33GSS2',(select codigo from modelo where nome = 'Uno'), (select codigo from grupo where nome = 'Básico')),
	('HGT4322',2020,'2020-04-22',null,			12345,'656456JH2H2G4333',(select codigo from modelo where nome = 'Fusca'), (select codigo from grupo where nome = 'Básico')),
	('BVT2233',2018,'2019-02-13',null,			65832,'17233665KJKJ7567',(select codigo from modelo where nome = 'Brasilia'), (select codigo from grupo where nome = 'Luxo')),
	('YTD2211',2017,'2017-09-29','2018-10-12',	80015,'2H43J234HG4J4J44',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Luxo')),
	('AST5544',2017,'2017-10-19',null		,	98015,'2H434434HG4J4J44',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Luxo')),
	('FFT5544',2018,'2018-05-12','2020-03-12',	45676,'2H43J234HG4J4J11',(select codigo from modelo where nome = 'Ka'), (select codigo from grupo where nome = 'Luxo')),
	('UYY5533',2017,'2017-12-20',null,			20015,'323J234HG4J4J441',(select codigo from modelo where nome = 'Golf'), (select codigo from grupo where nome = 'Luxo'))
