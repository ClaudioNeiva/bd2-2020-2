package br.ucsal.bes20202.bd2.aula12;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Exemplo {

	private static Connection connection;

	public static void main(String[] args) throws SQLException {
		connection = ConnectionUtil.getConnection();

		// exemploAutoCommit();

		// exemploUpdateCursor();

		exemploPersistParametro();

		// exemploInsertCursor();

		exemploListagem();
	}

	private static void exemploPersistParametro() throws SQLException {
		Correntista correntista1 = new Correntista("Italo");
		CorrentistaDAO.persist(correntista1);
		System.out.println("correntista1.id = " + correntista1.getId());
	}

	private static void exemploListagem() throws SQLException {
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		correntistas.stream().forEach(System.out::println);
	}

	private static void exemploAutoCommit() throws SQLException {
		connection.setAutoCommit(false);

		System.out.println("id do novo correntista=" + CorrentistaDAO.persist());

		// ConnectionUtil.getConnection().rollback();
		connection.commit();

		connection.setAutoCommit(true);
	}

	private static void exemploInsertCursor() throws SQLException {
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);) {
			rs.moveToInsertRow();
			rs.updateString("nm", "Leila");
			rs.insertRow();
		}
	}

	private static void exemploUpdateCursor() throws SQLException {
		String query = "select * from correntista order by nm asc";
		try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query)) {
			rs.next();
			rs.updateString("nm", "Manuela");
			rs.updateRow();
		}

	}

}
