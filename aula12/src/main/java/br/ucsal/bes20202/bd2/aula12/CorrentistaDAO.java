package br.ucsal.bes20202.bd2.aula12;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CorrentistaDAO {

	private static final String QUERY_FIND_ALL = "select id, nm from correntista order by nm asc";
	private static final String QUERY_INSERT_FIXO = "insert into correntista (nm) values ('Mauricio')";
	private static final String QUERY_INSERT = "insert into correntista (nm) values (?)";

	public static void persist(Correntista correntista) throws SQLException {
		try (PreparedStatement stmt = ConnectionUtil.getConnection().prepareStatement(QUERY_INSERT,
				Statement.RETURN_GENERATED_KEYS)) {
			stmt.setString(1, correntista.getNome());
			stmt.executeUpdate();
			correntista.setId(getGeneratedId(stmt));
		}
	}

	public static int persist() throws SQLException {
		try (Statement stmt = ConnectionUtil.getConnection().createStatement()) {
			stmt.executeUpdate(QUERY_INSERT_FIXO, Statement.RETURN_GENERATED_KEYS);
			return getGeneratedId(stmt);
		}
	}

	private static Integer getGeneratedId(Statement stmt) throws SQLException {
		try (ResultSet idRs = stmt.getGeneratedKeys();) {
			idRs.next();
			return idRs.getInt(1);
		}
	}

	public static List<Correntista> findAll() throws SQLException {
		List<Correntista> correntistas = new ArrayList<>();

		try (Statement stmt = ConnectionUtil.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				ResultSet.CONCUR_UPDATABLE); ResultSet rs = stmt.executeQuery(QUERY_FIND_ALL)) {
			rs.afterLast();
			while (rs.previous()) {
				correntistas.add(resultSet2Correntista(rs));
			}
		}

		return correntistas;
	}

	private static Correntista resultSet2Correntista(ResultSet rs) throws SQLException {
		Integer id = rs.getInt("id");
		String nome = rs.getString("nm");
		return new Correntista(id, nome);
	}

	public static List<Correntista> findAllAteJava6() throws SQLException {
		List<Correntista> correntistas = new ArrayList<>();

		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = ConnectionUtil.getConnection().createStatement();
			rs = stmt.executeQuery(QUERY_FIND_ALL);
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String nome = rs.getString("nm");
				correntistas.add(new Correntista(id, nome));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}

		return correntistas;
	}

}
