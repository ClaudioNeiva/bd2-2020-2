package br.ucsal.bes20202.bd2.aula12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

	private static final String URL = "jdbc:postgresql://localhost:5432/aula-jdbc";
	private static final String USER = "postgres";
	private static final String PASSWORD = "abcd1234";
	private static Connection connection;

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		}
		return connection;
	}

	public static void closeConnection() throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}
	
}
