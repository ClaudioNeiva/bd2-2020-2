package br.ucsal.bes20202.bd2.aula12;

public class Correntista {

	private Integer id;

	private String nome;

	public Correntista(String nome) {
		super();
		this.nome = nome;
	}

	public Correntista(Integer id, String nome) {
		this(nome);
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Correntista [id=" + id + ", nome=" + nome + "]";
	}

}
