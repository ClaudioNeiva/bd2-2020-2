package br.ucsal.bes20202.bd2.aula13;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("aula01jpa");
		EntityManager em = emf.createEntityManager();

		Aluno alunoA = new Aluno("Antonio");
		Aluno alunoB = new Aluno("Claudio");
		Aluno alunoC = new Aluno("Neiva");

		em.getTransaction().begin();
		em.persist(alunoA);
		em.persist(alunoB);
		em.persist(alunoC);
		em.getTransaction().commit();

		System.out.println("alunoA.matricula=" + alunoA.getMatricula());
		System.out.println("alunoB.matricula=" + alunoB.getMatricula());
		System.out.println("alunoC.matricula=" + alunoC.getMatricula());

		// System.out.println("Tecle ENTER para continuar...");
		// new Scanner(System.in).nextLine();

		// aluno = serializarDeserializarAluno(aluno);

		// System.out.println("em.contains(aluno)=" + em.contains(aluno));
		//
		// em.getTransaction().begin();
		// aluno.setNome("Pedro");
		// em.getTransaction().commit();

		emf.close();

	}

	private static Aluno serializarDeserializarAluno(Aluno aluno) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(aluno);
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		aluno = (Aluno) ois.readObject();
		return aluno;
	}

}
